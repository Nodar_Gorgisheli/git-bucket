import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-bar',
  templateUrl: './loading-bar.component.html',
  styleUrls: ['./loading-bar.component.css']
})
export class LoadingBarComponent implements OnInit {
  num = 0;
  constructor() { }

  ngOnInit(): void {
  }

  plusNumber() {
    this.num += 1;
    return this.num;
  }

  minusNumber() {
    this.num -= 1;
    return this.num;
  }

}
