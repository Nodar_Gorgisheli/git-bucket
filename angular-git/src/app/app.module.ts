import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { NumService } from './num.service';
import { LoadingBarComponent } from './loading-bar/loading-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    ButtonsComponent,
    LoadingBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [NumService],
  bootstrap: [AppComponent]
})
export class AppModule { }
