import { Component, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NumService {
  private num = 0;
  constructor() { }

  plusNumber() {
    this.num += 1;
    return this.num;
  }

  minusNumber() {
    this.num -= 1;
    return this.num;
  }

}
