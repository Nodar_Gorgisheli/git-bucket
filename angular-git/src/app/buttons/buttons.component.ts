import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NumService } from '../num.service';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.css']
})
export class ButtonsComponent implements OnInit {
  public n = 0;
  constructor(private numService: NumService) {
  }
  @Output() public plusMinusEvent = new EventEmitter();


  ngOnInit(): void {
    this.plusMinusEvent.emit(0);
  }

  onClickPlus() {
    this.plusMinusEvent.emit(this.n = this.numService.plusNumber());
  }

  onClickMinus() {
    this.plusMinusEvent.emit(this.n = this.numService.minusNumber());
  }

}
